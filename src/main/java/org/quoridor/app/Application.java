package org.quoridor.app;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 *
 * @author Igor
 */
@EnableAutoConfiguration
@ComponentScan(basePackages = "org.quoridor.app")
public class Application {

	@Bean(name = "hosts")
	public Map<String, ListHostDTO> listHosts() {
		return new ConcurrentHashMap<>();
	}
	
    public static void main(String args[]) {
        SpringApplication.run(Application.class, args);
    }
}
