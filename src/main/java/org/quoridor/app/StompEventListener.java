package org.quoridor.app;

import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

/**
 *
 * @author Igor
 */
@Component
public class StompEventListener implements ApplicationListener<SessionDisconnectEvent> {
 
    private final Log LOG = LogFactory.getLog(StompEventListener.class);
 
	@Autowired
	Map<String, ListHostDTO> hosts;
	
    @Override
    public void onApplicationEvent(SessionDisconnectEvent event) {
        StompHeaderAccessor sha = StompHeaderAccessor.wrap(event.getMessage());
		LOG.info("session id " + sha.getSessionId() + " disconnected");
        hosts.remove(sha.getSessionId());
    }
}
