package org.quoridor.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Controller;

/**
 *
 * @author devvm
 */
@Controller
public class GameController {

	@Autowired
	private Map<String, ListHostDTO> hosts;

	@MessageMapping("/create")
	@SendToUser("/response/create")
	public String createGame(StompHeaderAccessor sha, @Payload String name) {
		hosts.put(sha.getSessionId(), new ListHostDTO(name));
		return sha.getSessionId();
	}

	@MessageMapping("/join/{id}")
	@SendTo("/response/join/{id}")
	public String joinGame(StompHeaderAccessor sha, @DestinationVariable String id, @Payload String name) {
		if (hosts.containsKey(id)) {
			ListHostDTO host = hosts.get(id);
			host.setWaiting(false);
			return name;
		} else {
			return "error";
		}
	}

	@MessageMapping("/list")
	@SendToUser("/response/list")
	public List<HostDTO> getHosts() {
		List<HostDTO> list = new ArrayList<>();
		for (Map.Entry<String, ListHostDTO> entry : hosts.entrySet()) {
			ListHostDTO host = entry.getValue();
			if (host.isWaiting())
				list.add(new HostDTO(entry.getKey(), host.getName()));
		}
		return list;
	}
}
