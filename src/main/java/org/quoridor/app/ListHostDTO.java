package org.quoridor.app;

/**
 *
 * @author Igor
 */
public class ListHostDTO {
    
    private String name;
    private boolean waiting;

    public ListHostDTO(String id) {
	this.name = id;
	this.waiting = true;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public boolean isWaiting() {
	return waiting;
    }

    public void setWaiting(boolean waiting) {
	this.waiting = waiting;
    }
}
